﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;

namespace Bullet.Framework.Authentication.Models
{
    // Add profile data for application users by adding properties to the ApplicationUser class
    public class ApplicationUser : IdentityUser
    {
        public ApplicationUser() : base()
        {

        }

        public ApplicationUser(string userName) : base(userName)
        {

        }
    }
}
