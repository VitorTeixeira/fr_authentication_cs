﻿using IdentityServer4.Models;

namespace Bullet.Framework.Authentication.Models.ConsentViewModels
{
    public class ErrorViewModel
    {
        public ErrorMessage Error { get; set; }

    }
}
