﻿using System.Collections.Generic;

namespace Bullet.Framework.Authentication.Models.ConsentViewModels
{
    public class ConsentInputModel
    {
        public string Button { get; set; }
        public IEnumerable<string> ScopesConsented { get; set; }
        public bool RememberConsent { get; set; }
        public string ReturnUrl { get; set; }
    }
}
