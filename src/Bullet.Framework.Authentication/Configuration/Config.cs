﻿using IdentityServer4;
using IdentityServer4.Models;
using IdentityServer4.Test;
using System;
using System.Collections.Generic;
using System.Security.Claims;

namespace Bullet.Framework.Authentication.Configuration
{
    public class Config
    {
        // scopes define the resources in your system
        public static IEnumerable<IdentityResource> GetIdentityResources()
        {
            return new List<IdentityResource>
            {
                new IdentityResources.OpenId(), //subject id
                new IdentityResources.Profile(), //first name, last name etc..
                new IdentityResources.Address(),
                new IdentityResources.Email(),
                new IdentityResources.Phone(),
            };
        }

        public static IEnumerable<ApiResource> GetApiResources()
        {
            return new List<ApiResource>
            {
                new ApiResource {
                    Name = "bestevo_api",
                    DisplayName = "BestEvoApi",
                    Description =  "used for nothing",
                    Scopes = { new Scope("bestevo_api", "BestEvolution Super Service") },
                    UserClaims = new []{ "role", "student", "admin" }
                },
            };
        }


        // clients want to access resources (aka scopes)
        public static IEnumerable<Client> GetClients()
        {
            // client credentials client
            return new List<Client>
            {         
                // JavaScript Client
                new Client
                {
                    ClientId = "js_oidc",
                    ClientName = "JavaScript Client",
                    AllowedGrantTypes = GrantTypes.Implicit,
                    AllowAccessTokensViaBrowser = true,

                    RedirectUris = { "http://localhost:7017/callback.html" },
                    PostLogoutRedirectUris = { "http://localhost:7017/index.html" },
                    AllowedCorsOrigins = { "http://localhost:7017" },

                    AllowedScopes =
                    {
                        IdentityServerConstants.StandardScopes.OpenId,
                        IdentityServerConstants.StandardScopes.Profile,
                        IdentityServerConstants.StandardScopes.Email,
                        "bestevo_api"
                    }
                }
            };
        }

        public static List<TestUser> GetUsers()
        {
            return new List<TestUser>
            {
                new TestUser
                {
                    SubjectId = Guid.NewGuid().ToString(),
                    Username = "alice",
                    Password = "password",

                    Claims = new List<Claim>
                    {
                        new Claim(ClaimTypes.GivenName,"Furtado"),
                        new Claim("name", "Alice"),
                        new Claim("website", "https://alice.com"),
                        new Claim("student","create"),
                        new Claim("student","read"),
                        new Claim("student","update"),
                        new Claim("student","delete"),
                        new Claim("admin","read"),
                        new Claim("admin","update"),
                        new Claim("role", "student")

                    }
                },
                new TestUser
                {
                    SubjectId = Guid.NewGuid().ToString(),
                    Username = "bob",
                    Password = "password",
                    Claims = new List<Claim>
                    {
                        new Claim("name", "Bob"),
                        new Claim("website", "https://bob.com"),
                        new Claim("student","create"),
                        new Claim("student","read"),
                        new Claim("role", "admin"),
                        new Claim("role", "student"),
                        new Claim("admin","read"),
                        new Claim("admin","update"),
                    }
                }
            };
        }
    }
}
